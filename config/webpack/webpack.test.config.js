/*globals require, module */

var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  resolve: {
    extensions: ['.js', '.ts']
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: [
          'awesome-typescript-loader',
          'angular2-template-loader'
        ]
      },
      {
        test: /\.pug$/,
        loader: ['raw-loader', 'pug-html-loader?pretty']
      },
      {
        test: /\.scss$/,
        loader: 'raw-loader!sass-loader'
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([{
      from: 'src/resources',
      to: 'resources'
    }])
  ]
};
