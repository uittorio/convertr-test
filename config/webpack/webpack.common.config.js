/*globals require, module */

var webpack = require('webpack'),
  root = require('../root/root'),
  CopyWebpackPlugin = require('copy-webpack-plugin'),
  HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: [
          'awesome-typescript-loader',
          'angular2-template-loader',
          'angular-router-loader'
        ]
      },
      {
        test: /\.pug$/,
        loader: ['raw-loader', 'pug-html-loader?pretty']
      },
      {
        test: /\.scss$/,
        loader: 'raw-loader!sass-loader'
      },
      // Ignore warnings about System.import in Angular
      { test: /[\/\\]@angular[\/\\].+\.js$/, parser: { system: true } }
    ]
  },
  entry: {
    vendors: './src/libraries.ts',
    polyfills: './src/polyfills.ts',
    app: './src/main/main.ts'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      }
    }
  },
  resolve: {
    modules: [
      root('src'),
      'node_modules'
    ],
    extensions: ['.js', '.ts']
  },
  plugins: [
    new CopyWebpackPlugin([{
      from: 'src/resources',
      to: 'resources'
    }]),
    new HtmlWebpackPlugin({
      template: 'src/index.pug'
    }),
    new webpack.ContextReplacementPlugin(
      /angular(\\|\/)core(\\|\/)(@angular|esm5)/,
      root('src')
    )
  ]
};
