var webpackConfig = require('../webpack/webpack.test.config');

module.exports = function(config) {
  var _config = {
    basePath: '',

    frameworks: ['jasmine'],

    files: [
      { pattern: './config/webpack/webpack-require-context.js', watched: false },
      { pattern: './src/resources/images/*.svg', included: false, served: true, watched: false  }
    ],

    preprocessors: {
      './config/webpack/webpack-require-context.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      stats: 'errors-only'
    },

    webpackServer: {
      noInfo: true
    },

    reporters: ['dots'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: ['ChromeHeadless']
  };

  config.set(_config);
};
