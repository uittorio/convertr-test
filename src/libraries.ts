import '@angular/common';
import '@angular/compiler';
import '@angular/core';
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/router';
import '@ngx-translate/core';

import 'rxjs';
import 'lodash';
