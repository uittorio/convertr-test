import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutesModule } from "./app.routes.module";
import { AppComponent } from "./app.component";
import { CoreModule } from "./core/core.module";
import { RouterModule } from "@angular/router";
import { HomeModule } from "./home/home.module";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { HtmlImageLoader } from "./browser/htmlImageLoader/htmlImageLoader";
import { Preloader } from "./core/preloader/preloader";
import { GraphicRepository } from "./core/graphic/graphicRepository/graphicRepository";
import { GraphicLoader } from "./core/graphic/graphicLoader/graphicLoader";
import { ModelApiRequester } from "./core/model/modelApiRequester/modelApiRequester";
import { ModelRequester } from "./core/model/modelRequester/modelRequester";

@NgModule({
	imports: [
		BrowserModule,
		RouterModule,
		AppRoutesModule,
		HttpClientModule,
		CoreModule,
		HomeModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (httpClient: HttpClient) => {
					return new TranslateHttpLoader(httpClient, "/resources/i18n/", ".json");
				},
				deps: [HttpClient]
			}
		})
	],
	providers: [
		GraphicRepository,
		{
			provide: GraphicLoader,
			useClass: HtmlImageLoader
		},
		Preloader,
		{
			provide: ModelRequester,
			useFactory: (httpClient: HttpClient) => {
				return new ModelApiRequester(httpClient);
			},
			deps: [HttpClient]
		}
	],
	declarations: [AppComponent],
	bootstrap: [AppComponent]
})

export class AppModule {
}
