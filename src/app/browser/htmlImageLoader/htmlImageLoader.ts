import { GraphicLoader } from "../../core/graphic/graphicLoader/graphicLoader";
import { GraphicName } from "../../core/graphic/graphicName/graphicName";
import { GraphicRepository } from "../../core/graphic/graphicRepository/graphicRepository";
import { Graphic } from "../../core/graphic/graphic";
import { GraphicResources } from "../../core/graphic/graphicResources/graphicResources";
import { Injectable } from "@angular/core";

@Injectable()
export class HtmlImageLoader implements GraphicLoader {
	private _graphicRepository: GraphicRepository;
	
	constructor(graphicRepository: GraphicRepository) {
		this._graphicRepository = graphicRepository;
	}
	
	public load(graphicName: GraphicName): Promise<Graphic> {
		const image: HTMLImageElement = new Image();
		const imageUrl: string = GraphicResources[graphicName];
		
		return new Promise((resolve: Function, reject: Function) => {
			image.onload = () => {
				let graphic: Graphic = Graphic.create(graphicName, imageUrl);
				
				this._graphicRepository.set(graphic);
				
				resolve(graphic);
			};
			
			image.onerror = (e) => {
				reject(e);
			};
			
			image.src = imageUrl;
		});
	}
	
}