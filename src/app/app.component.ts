import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Preloader } from "./core/preloader/preloader";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: 'cr-app',
	templateUrl: './app.pug',
	styleUrls: ['./app.scss'],
	encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
	public isLoading: boolean;
	private _preloader: Preloader;
	private _translate: TranslateService;
	
	constructor(preloader: Preloader, translate: TranslateService) {
		this._preloader = preloader;
		this._translate = translate;
		this.isLoading = true;
		translate.use('en');
		translate.setDefaultLang('en');
	}
	
	public ngOnInit(): void {
		this._preloadImages();
	}
	
	private async _preloadImages(): Promise<void> {
		await this._preloader.loadImages();
		
		this.isLoading = false;
	}
}
