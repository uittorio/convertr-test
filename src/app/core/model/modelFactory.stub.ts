import { ModelFactory } from "./modelFactory";

export class ModelFactoryStub<TModel, TData> implements ModelFactory<TModel, TData> {
	create: (data: TData) =>  TModel = jasmine.createSpy('create');
}