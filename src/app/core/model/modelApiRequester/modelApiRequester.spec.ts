import { ModelApiRequester } from "./modelApiRequester";
import { HttpStub } from "../../http/http.stub";
import { ModelFactoryStub } from "../modelFactory.stub";
import { ModelCollectionData } from "../modelCollectionData/modelCollectionData";
import { Subject } from "rxjs/Subject";
import { async } from "@angular/core/testing";

interface IModel {}
interface MyModelData {}
describe("ModelApiRequester", () => {
	let modelApiRequester: ModelApiRequester,
		httpStub: HttpStub,
		modelFactory: ModelFactoryStub<IModel, MyModelData>,
		modelFromFactory: MyModelData;
	
	beforeEach(() => {
		modelFactory = new ModelFactoryStub();
		httpStub = new HttpStub();
		
		modelFromFactory = {};
		
		modelApiRequester = new ModelApiRequester(httpStub);
		(modelFactory.create as jasmine.Spy).and.returnValue(modelFromFactory);
	});
	
	describe("when getting collection", () => {
		let list: Array<IModel>,
			httpGetSubject: Subject<ModelCollectionData<MyModelData>>;
			
		beforeEach(async(() => {
			httpGetSubject = new Subject();
			(httpStub.get as jasmine.Spy).and.returnValue(httpGetSubject);
			modelApiRequester.getCollectionData(modelFactory, '/url', {}).then((result: Array<IModel>) => {
				list = result;
			});
			httpGetSubject.next({
				"hydra:member": [
					modelFromFactory,
					modelFromFactory
				]
			});
			
			httpGetSubject.complete();
		}));
		
		it('should get collection data through the http get', () => {
			expect(httpStub.get).toHaveBeenCalledWith('https://arkham.cvtr.io/test/api/url', { params: { }});
		});
		
		it('should adapt the data from hydra and create a list of model', () => {
			expect(list).toEqual([
				modelFromFactory,
				modelFromFactory
			]);
		});
	});
	
	describe("when getting data", () => {
		let model: IModel,
			httpGetSubject: Subject<MyModelData>;
		
		beforeEach(async(() => {
			httpGetSubject = new Subject();
			(httpStub.get as jasmine.Spy).and.returnValue(httpGetSubject);
			modelApiRequester.get(modelFactory, '/url', {}).then((result: IModel) => {
				model = result;
			});
			
			httpGetSubject.next({});
			
			httpGetSubject.complete();
		}));
		
		it('should get the data through the http get', () => {
			expect(httpStub.get).toHaveBeenCalledWith('https://arkham.cvtr.io/test/api/url', { params: { }});
		});
		
		it('should adapt the data from hydra and create a model', () => {
			expect(model).toBe(modelFromFactory);
		});
	});
	
	describe("when creating data", () => {
		let model: IModel,
			httpPostSubject: Subject<MyModelData>;
		
		beforeEach(async(() => {
			httpPostSubject = new Subject();
			(httpStub.post as jasmine.Spy).and.returnValue(httpPostSubject);
			modelApiRequester.post(modelFactory, {}, '/url', {}).then((result: IModel) => {
				model = result;
			});
			
			httpPostSubject.next({});
			
			httpPostSubject.complete();
		}));
		
		it('should post the data through the http get', () => {
			expect(httpStub.post).toHaveBeenCalledWith('https://arkham.cvtr.io/test/api/url', {}, { params: { }});
		});
		
		it('should adapt the data from hydra and create a model', () => {
			expect(model).toBe(modelFromFactory);
		});
	});
});