import { IHttp } from "../../http/http";
import { ModelFactory } from "../modelFactory";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { ModelCollectionData } from "../modelCollectionData/modelCollectionData";
import { IDict } from "../../dictionary/dictionary";
import { ModelRequester } from "../modelRequester/modelRequester";
import { HttpHeaders } from "@angular/common/http";

export class ModelApiRequester implements ModelRequester {
	private readonly _baseUrl: string = 'https://arkham.cvtr.io/test/api';
	private _http: IHttp;
	
	constructor(http: IHttp) {
		this._http = http;
	}
	
	public getCollectionData<TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, modelUrl: string, params: IDict<string>): Promise<Array<TModel>> {
		const url: string = this._baseUrl + modelUrl;
		
		return this._http.get<ModelCollectionData<TModelData>>(url, { params: params })
			.map((response: ModelCollectionData<TModelData>) => {
				return response['hydra:member'].map((modelData: TModelData) => {
					return modelFactory.create(modelData || {} as TModelData);
				});
		}).toPromise();
	}
	
	public get<TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, modelUrl: string, params: IDict<string>): Promise<TModel> {
		const url: string = this._baseUrl + modelUrl;
		
		return this._http.get<TModelData>(url, { params: params })
			.map((response: TModelData) => {
				return modelFactory.create(response || {} as TModelData);
			}).toPromise();
	}
	
	public post<TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, model: TModelData, modelUrl: string, params: IDict<string>): Promise<TModel> {
		const url: string = this._baseUrl + modelUrl;
		
		return this._http.post<TModelData>(url, model, { params: params})
			.map((response: TModelData) => {
				return modelFactory.create(response || {} as TModelData);
			}).toPromise();
	}
}