export interface ModelCollectionData<TData> {
	'hydra:member': Array<TData>;
}