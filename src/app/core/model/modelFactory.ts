export interface ModelFactory<TModel, TModelData> {
	create(data: TModelData): TModel;
}