export interface Model<T> {
	attributes: T;
}