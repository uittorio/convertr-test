import { ModelFactory } from "../modelFactory";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { IDict } from "../../dictionary/dictionary";
import { ModelRequester } from "./modelRequester";

export class ModelRequesterStub implements ModelRequester {
	public getCollectionData: <TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, url: string, params: IDict<string>) => Promise<Array<TModel>> = jasmine.createSpy('getCollectionData');
	public get: <TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, url: string, params: IDict<string>) => Promise<TModel> = jasmine.createSpy('get');
	public post: <TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, model: TModelData, modelUrl: string, params: IDict<string>) => Promise<TModel> = jasmine.createSpy('post');
}