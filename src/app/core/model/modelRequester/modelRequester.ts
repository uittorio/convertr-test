import { IDict } from "../../dictionary/dictionary";
import { ModelFactory } from "../modelFactory";

export abstract class ModelRequester {
	public abstract getCollectionData<TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, url: string, params: IDict<string>): Promise<Array<TModel>>;
	public abstract get<TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, url: string, params: IDict<string>): Promise<TModel>;
	public abstract post<TModel, TModelData>(modelFactory: ModelFactory<TModel, TModelData>, model: TModelData, modelUrl: string, params: IDict<string>): Promise<TModel>;
}