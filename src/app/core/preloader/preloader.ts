import { GraphicNamePreload } from "../graphic/graphicNamePreload/graphicNamePreload";
import { Graphic } from "../graphic/graphic";
import { GraphicName } from "../graphic/graphicName/graphicName";
import { GraphicLoader } from "../graphic/graphicLoader/graphicLoader";
import { Injectable } from "@angular/core";

@Injectable()
export class Preloader {
	private _graphicLoader: GraphicLoader;
	
	constructor(graphicLoader: GraphicLoader) {
		this._graphicLoader = graphicLoader;
	}
	
	public loadImages(): Promise<Array<Graphic>> {
		let graphicPromises: Array<Promise<Graphic>> = GraphicNamePreload.map((graphic: GraphicName) => {
			return this._graphicLoader.load(graphic);
		});
		
		return Promise.all(graphicPromises);
	}
}