import { async } from "@angular/core/testing";
import { Preloader } from "./preloader";
import { GraphicLoaderStub } from "../graphic/graphicLoader/graphicLoader.stub";
import { Graphic } from "../graphic/graphic";
import { GraphicStub } from "../graphic/graphic.stub";
import { GraphicName } from "../graphic/graphicName/graphicName";

describe("Preloader", () => {
	let preloader: Preloader,
		graphicLoader: GraphicLoaderStub,
		promiseImageResolve: Function;
	
	beforeEach(() => {
		const promise: Promise<Graphic> = new Promise((resolve: Function) => {
			promiseImageResolve = resolve;
		});
		
		graphicLoader = new GraphicLoaderStub();
		preloader = new Preloader(graphicLoader);
		
		(graphicLoader.load as jasmine.Spy).and.returnValue(promise)
	});
	
	describe("when all promises resolve", () => {
		let graphics: Array<Graphic>,
			graphicStub: GraphicStub;
		
		beforeEach(async(() => {
			graphicStub = new GraphicStub();
			preloader.loadImages().then((result: Array<Graphic>) => {
				graphics = result;
			});
			promiseImageResolve(graphicStub);
		}));
		
		it('should return the graphics', () => {
			expect(graphicLoader.load).toHaveBeenCalledWith(GraphicName.LOGO);
			expect(graphics).toEqual([graphicStub]);
		});
	});
});