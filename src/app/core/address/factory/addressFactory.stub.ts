import { ModelFactory } from "../../model/modelFactory";
import { Address } from "../address";
import { AddressData } from "../addressData";

export class AddressFactoryStub implements ModelFactory<Address, AddressData> {
	public create: (data: AddressData) => Address = jasmine.createSpy('create');
}