import { Address } from "../address";
import { AddressStub } from "../address.stub";
import { AddressFactory } from "./addressFactory";

describe("AddressFactory", () => {
	let addressFactory: AddressFactory,
		addressStub: AddressStub;
	
	beforeEach(() => {
		addressFactory = new AddressFactory();
		addressStub = new AddressStub();
		spyOn(Address, 'create').and.returnValue(addressStub);
	});
	
	it('should create an address with the right data', function () {
		const address: Address = addressFactory.create({
			postcode: 'postcode',
			address: 'address'
		});
		
		expect(address).toBe(addressStub);
		expect(Address.create).toHaveBeenCalledWith({
			postcode: 'postcode',
			address: 'address'
		})
	});
});