import { ModelFactory } from "../../model/modelFactory";
import { Address } from "../address";
import { AddressData } from "../addressData";
import { Injectable } from "@angular/core";

@Injectable()
export class AddressFactory implements ModelFactory<Address, AddressData> {
	public create(data: AddressData): Address {
		return Address.create(data);
	}
}