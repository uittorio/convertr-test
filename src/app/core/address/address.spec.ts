import { Address } from "./address";

describe("Address", () => {
	it('should create an address with the right data', function () {
		const address: Address = Address.create({
			postcode: 'postcode',
			address: 'address'
		});
		
		expect(address.attributes).toEqual({
			postcode: 'postcode',
			address: 'address'
		});
	});
});