import { ModelRequester } from "../../model/modelRequester/modelRequester";
import { AddressFactory } from "../factory/addressFactory";
import { Address } from "../address";
import { Injectable } from "@angular/core";
import { AddressData } from "../addressData";

@Injectable()
export class AddressRequester {
	private _addressFactory: AddressFactory;
	private _modelRequester: ModelRequester;
	
	constructor(modelRequester: ModelRequester, addressFactory: AddressFactory) {
		this._addressFactory = addressFactory;
		this._modelRequester = modelRequester;
	}
	
	public get(addressEndpoint: string): Promise<Address> {
		return this._modelRequester.get(this._addressFactory, addressEndpoint, {});
	}
	
	public create(address: AddressData): Promise<Address> {
		return this._modelRequester.post(this._addressFactory, address, '/addresses', {});
	}
}
