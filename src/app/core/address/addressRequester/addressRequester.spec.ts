import { async } from "@angular/core/testing";
import { ModelRequesterStub } from "../../model/modelRequester/modelRequester.stub";
import { AddressRequester } from "./addressRequester";
import { AddressFactoryStub } from "../factory/addressFactory.stub";
import { Address } from "../address";
import { AddressStub } from "../address.stub";

describe("AddressRequester", () => {
	let addressRequester: AddressRequester,
		modelRequester: ModelRequesterStub,
		addressFactory: AddressFactoryStub
	
	beforeEach(async(() => {
		modelRequester = new ModelRequesterStub();
		addressFactory = new AddressFactoryStub();
		addressRequester = new AddressRequester(modelRequester, addressFactory);
	}));
	
	describe("when getting an address", () => {
		let addressResult: Address,
			addressFetched: Address,
			resolveRequester: Function;
		
		beforeEach(async(() => {
			(modelRequester.get as jasmine.Spy).and.returnValue(new Promise((resolve: Function) => {
				resolveRequester = resolve;
			}));
			
			addressRequester.get('an/endpoint').then((address: Address) => {
				addressFetched = address;
			});
			
			addressResult = new AddressStub();
			
			resolveRequester(addressResult);
		}));
		
		it("should get data from the requester with no params", () => {
			expect(modelRequester.get).toHaveBeenCalledWith(addressFactory, 'an/endpoint', {});
		});
		
		it("should return the address", () => {
			expect(addressFetched).toBe(addressResult);
		});
	});
	
	describe("when creating an address", () => {
		let addressResult: Address,
			addressCreated: Address,
			resolveRequester: Function;
		
		beforeEach(async(() => {
			(modelRequester.post as jasmine.Spy).and.returnValue(new Promise((resolve: Function) => {
				resolveRequester = resolve;
			}));
			
			addressRequester.create({
				postcode: 'postcode',
				address: 'address'
			}).then((address: Address) => {
				addressCreated = address;
			});
			
			addressResult = new AddressStub();
			
			resolveRequester(addressResult);
		}));
		
		it("should post the data to the requester with no params", () => {
			expect(modelRequester.post).toHaveBeenCalledWith(addressFactory, {
				postcode: 'postcode',
				address: 'address'
			}, '/addresses', {});
		});
		
		it("should return the address", () => {
			expect(addressCreated).toBe(addressResult);
		});
	});
});