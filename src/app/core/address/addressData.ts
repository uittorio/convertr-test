export interface AddressData {
	address: string;
	postcode: string;
}