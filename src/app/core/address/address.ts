import { Model } from "../model/model";
import { AddressData } from "./addressData";

export class Address implements Model<AddressData> {
	public attributes: AddressData;
	
	private constructor(data: AddressData) {
		this.attributes = {
			address: data.address,
			postcode: data.postcode
		};
	}
	
	public static create(data: AddressData): Address {
		return new Address(data);
	}
}