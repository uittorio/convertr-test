import { Model } from "../model/model";
import { AddressData } from "./addressData";

export class AddressStub implements Model<AddressData> {
	public attributes: AddressData;
}