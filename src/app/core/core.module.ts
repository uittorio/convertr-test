import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";
import { AdvertiserRequester } from "./advertiser/advertiserRequester/advertiserRequester";
import { AdvertiserFactory } from "./advertiser/factory/advertiserFactory";
import { AdvertiserTableData } from "./advertiser/advertiserTableData/advertiserTableData";
import { AddressFactory } from "./address/factory/addressFactory";
import { AddressRequester } from "./address/addressRequester/addressRequester";
import { AdvertiserDialogComponent } from "./advertiser/advertiserDialog/advertiserDialog.component";
import { AdvertiserForm } from "./advertiser/advertiserForm/advertiserForm";

@NgModule({
	imports: [
		SharedModule
	],
	declarations: [
		AdvertiserDialogComponent
	],
	exports: [
		AdvertiserDialogComponent
	],
	providers: [
		AdvertiserFactory,
		AdvertiserRequester,
		AdvertiserTableData,
		AddressFactory,
		AddressRequester,
		AdvertiserForm
	]
})

export class CoreModule {
}
