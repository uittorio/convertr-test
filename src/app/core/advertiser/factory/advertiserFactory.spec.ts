import { async } from "@angular/core/testing";
import { Advertiser } from "../advertiser";
import { AdvertiserStub } from "../advertiser.stub";
import { AdvertiserFactory } from "./advertiserFactory";

describe("AdvertiserFactory", () => {
	let advertiserFactory: AdvertiserFactory,
		advertiserStub: AdvertiserStub;
	
	beforeEach(async(() => {
		advertiserFactory = new AdvertiserFactory();
		advertiserStub = new AdvertiserStub();
		spyOn(Advertiser, 'create').and.returnValue(advertiserStub);
	}));
	
	it('should create an advertiser with the right data', function () {
		const advertiser: AdvertiserStub = advertiserFactory.create({
			name: 'name',
			orgurl: 'orgurl',
			address: 'address'
		});
		
		expect(advertiser).toBe(advertiserStub);
		expect(Advertiser.create).toHaveBeenCalledWith({
			name: 'name',
			orgurl: 'orgurl',
			address: 'address'
		})
	});
});