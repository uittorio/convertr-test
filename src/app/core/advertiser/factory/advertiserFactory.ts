import { ModelFactory } from "../../model/modelFactory";
import { Advertiser } from "../advertiser";
import { AdvertiserData } from "../advertiserData";
import { Injectable } from "@angular/core";

@Injectable()
export class AdvertiserFactory implements ModelFactory<Advertiser, AdvertiserData> {
	public create(data: AdvertiserData): Advertiser {
		return Advertiser.create(data);
	}
}