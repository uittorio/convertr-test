import { ModelFactory } from "../../model/modelFactory";
import { Advertiser } from "../advertiser";
import { AdvertiserData } from "../advertiserData";

export class AdvertiserFactoryStub implements ModelFactory<Advertiser, AdvertiserData> {
	public create: (data: AdvertiserData) => Advertiser = jasmine.createSpy('create');
}