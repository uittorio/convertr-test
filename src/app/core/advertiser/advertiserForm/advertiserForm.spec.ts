import { AdvertiserForm } from "./advertiserForm";

describe("AdvertiserForm", () => {
	let advertiserForm: AdvertiserForm;
	
	beforeEach(() => {
		advertiserForm = new AdvertiserForm();
	});
	
	it('should return the form configuration', function () {
		expect(advertiserForm.get()).toEqual([
			{
				placeholder: {
					translateId: "advertiser.add.name.placeholder"
				},
				value: '',
				type: 'text',
				name: 'name',
				validators: [
					jasmine.any(Function),
					jasmine.any(Function)
				]
			},
			{
				placeholder: {
					translateId: "advertiser.add.orgurl.placeholder"
				},
				value: '',
				type: 'text',
				name: 'url',
				validators: [
					jasmine.any(Function),
					jasmine.any(Function),
					jasmine.any(Function)
				]
			},
			{
				placeholder: {
					translateId: "advertiser.add.address.placeholder"
				},
				value: '',
				type: 'text',
				name: 'address',
				validators: [
					jasmine.any(Function),
					jasmine.any(Function)
				]
			},
			{
				placeholder: {
					translateId: "advertiser.add.postcode.placeholder"
				},
				value: '',
				type: 'text',
				name: 'postcode',
				validators: [
					jasmine.any(Function),
					jasmine.any(Function),
				]
			},
		]);
	});
});