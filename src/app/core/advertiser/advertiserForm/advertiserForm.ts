import { FormInputConfiguration } from "../../../shared/form/formInput/formInputConfiguration";
import { CRValidatorsService } from "../../validators/validatorService";
import { IDict } from "../../dictionary/dictionary";

export class AdvertiserForm {
	get(): Array<FormInputConfiguration> {
		return [
			{
				placeholder: {
					translateId: "advertiser.add.name.placeholder"
				},
				value: '',
				type: 'text',
				name: 'name',
				validators: [
					CRValidatorsService.maxLength(10),
					CRValidatorsService.required
				]
			},
			{
				placeholder: {
					translateId: "advertiser.add.orgurl.placeholder"
				},
				value: '',
				type: 'text',
				name: 'url',
				validators: [
					CRValidatorsService.maxLength(255),
					CRValidatorsService.required,
					CRValidatorsService.url
				]
			},
			{
				placeholder: {
					translateId: "advertiser.add.address.placeholder"
				},
				value: '',
				type: 'text',
				name: 'address',
				validators: [
					CRValidatorsService.maxLength(10),
					CRValidatorsService.required
				]
			},
			{
				placeholder: {
					translateId: "advertiser.add.postcode.placeholder"
				},
				value: '',
				type: 'text',
				name: 'postcode',
				validators: [
					CRValidatorsService.required,
					CRValidatorsService.maxLength(10)
				]
			},
		];
	}
}