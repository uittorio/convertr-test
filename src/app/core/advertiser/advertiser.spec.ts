import { Advertiser } from "./advertiser";

describe("Advertiser", () => {
	it('should create an address with the right data', function () {
		const advertiser: Advertiser = Advertiser.create({
			name: 'name',
			orgurl: 'orgurl',
			address: 'address'
		});
		
		expect(advertiser.attributes).toEqual({
			name: 'name',
			orgurl: 'orgurl',
			address: 'address'
		});
	});
	
	it('should be possible to add virtual fields', () => {
		const advertiser: Advertiser = Advertiser.create({
			name: 'name',
			orgurl: 'orgurl',
			address: 'address'
		});
		
		advertiser.setVirtualField('addressName', 'addressNameValue');
		advertiser.setVirtualField('postcode', 'postcodeValue');
		
		expect(advertiser.virtualField.addressName).toBe('addressNameValue');
		expect(advertiser.virtualField.postcode).toBe('postcodeValue');
	});
});