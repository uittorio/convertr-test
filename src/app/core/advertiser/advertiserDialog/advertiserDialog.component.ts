import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from "rxjs/Subject";
import { FormConfiguration } from "../../../shared/form/formConfiguration";
import { AdvertiserForm } from "../advertiserForm/advertiserForm";
import { FormGroup } from "@angular/forms";
import { AddressRequester } from "../../address/addressRequester/addressRequester";
import { AdvertiserRequester } from "../advertiserRequester/advertiserRequester";
import { Advertiser } from "../advertiser";

@Component({
	selector: 'cr-advertiser-dialog',
	templateUrl: './advertiserDialog.pug',
	styleUrls: ['./advertiserDialog.scss']
})

export class AdvertiserDialogComponent implements OnInit {
	@Input()
	public crAdvertiserDialogOpen: Subject<boolean>;
	
	@Output()
	public crAdvertiserDialogAfterCreate: EventEmitter<Advertiser> = new EventEmitter<Advertiser>();
	
	public isOpen: boolean = false;
	public isSaving: boolean = false;
	public formConfiguration: FormConfiguration;
	private _advertiserForm: AdvertiserForm;
	private _addressRequester: AddressRequester;
	private _advertiserRequester: AdvertiserRequester;
	
	constructor(advertiserForm: AdvertiserForm, addressRequester: AddressRequester, advertiserRequester: AdvertiserRequester) {
		this._advertiserForm = advertiserForm;
		this._addressRequester = addressRequester;
		this._advertiserRequester = advertiserRequester;
	}
	
	public closeDialog(): void {
		this.crAdvertiserDialogOpen.next(false);
	}
	
	public ngOnInit(): void {
		this.crAdvertiserDialogOpen.subscribe((open: boolean) => {
			this.isOpen = open;
		});
		
		this.formConfiguration = {
			submit: (form: FormGroup) => this._saveAdvertiser(form.value),
			inputs: this._advertiserForm.get()
		}
	}
	
	private _saveAdvertiser(value) {
		this.isSaving = true;
		this._addressRequester.create({
			address: value.address,
			postcode: value.postcode
		}).then(() => {
			this._advertiserRequester.create({
				name: value.name,
				orgurl: value.url,
				address: '/addresses/4' //TODO WHEN POST REQUEST WORKS THIS ONE NEEDS TO BE REPLACE WITH THE ADDRESS REAL ID JUST GENERATED
			}).then((advertiser: Advertiser) => {
				this.isSaving = false;
				advertiser.attributes.orgurl = value.url; //TODO WHEN POST REQUEST WORKS REPLACE THIS WITH RETURN VALUE ADVERTISER
				advertiser.attributes.name = value.name; //TODO WHEN POST REQUEST WORKS REPLACE THIS WITH RETURN VALUE ADVERTISER
				advertiser.attributes.address = '/addresses/4'; //TODO WHEN POST REQUEST WORKS REPLACE THIS WITH RETURN VALUE ADVERTISER
				advertiser.setVirtualField('addressName', value.address);
				advertiser.setVirtualField('postcode', value.postcode);

				this.closeDialog();
				this.crAdvertiserDialogAfterCreate.emit(advertiser);
			});
		});
	}
}