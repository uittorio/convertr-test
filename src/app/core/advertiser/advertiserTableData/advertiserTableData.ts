import { TableHeaderConfiguration } from "../../../shared/table/tableHeader/tableHeaderConfiguration";
import { Injectable } from "@angular/core";
import { TableRowValueConfiguration } from "../../../shared/table/tableRow/tableRowValueConfiguration";
import { Advertiser } from "../advertiser";
import { AdvertiserViewData } from "../advertiserViewData";

@Injectable()
export class AdvertiserTableData {
	public tableDataHeaders: Array<TableHeaderConfiguration<AdvertiserViewData>>;
	
	constructor() {
		this.tableDataHeaders = [
			{
				key: "name",
				value: {
					translateId: "advertiser.header.name"
				},
			},
			{
				key: "orgurl",
				value: {
					translateId: "advertiser.header.url"
				},
			},
			{
				key: "addressName",
				value: {
					translateId: "advertiser.header.address"
				},
			},
			{
				key: "postcode",
				value: {
					translateId: "advertiser.header.postcode"
				}
			}
		];
	}
	
	public generateTableDataRows(data: Advertiser): TableRowValueConfiguration {
		let rowValue: TableRowValueConfiguration = {};
		
		this.tableDataHeaders.forEach((header: TableHeaderConfiguration<AdvertiserViewData>) => {
			rowValue[header.key] = data.attributes[header.key] || data.virtualField[header.key];
		});
		
		return rowValue;
	}
}