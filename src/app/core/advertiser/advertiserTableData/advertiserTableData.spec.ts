import { AdvertiserTableData } from "./advertiserTableData";
import { AdvertiserStub } from "../advertiser.stub";

describe("AdvertiserTableData", () => {
	let advertiserTableData: AdvertiserTableData;
	
	beforeEach(() => {
		advertiserTableData = new AdvertiserTableData();
	});
	
	it('should create headers for advertiser', function () {
		expect(advertiserTableData.tableDataHeaders).toEqual([
			{
				key: "name",
				value: {
					translateId: "advertiser.header.name"
				},
			},
			{
				key: "orgurl",
				value: {
					translateId: "advertiser.header.url"
				},
			},
			{
				key: "addressName",
				value: {
					translateId: "advertiser.header.address"
				},
			},
			{
				key: "postcode",
				value: {
					translateId: "advertiser.header.postcode"
				}
			}
		]);
	});
	
	it('should create rows for advertiser', function () {
		const advertiser: AdvertiserStub = new AdvertiserStub();
		advertiser.attributes = {
			name: 'name',
			orgurl: 'orgurl',
			address: 'address'
		};
		advertiser.virtualField = {
			addressName: 'addressName',
			postcode: 'postcode'
		};
		
		expect(advertiserTableData.generateTableDataRows(advertiser)).toEqual({ name: 'name', orgurl: 'orgurl', addressName: 'addressName', postcode: 'postcode' });
	});
});