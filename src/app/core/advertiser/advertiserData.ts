export interface AdvertiserData {
	name: string;
	orgurl: string;
	address: string;
}