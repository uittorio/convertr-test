export interface AdvertiserVirtualData {
	addressName: string;
	postcode: string;
}