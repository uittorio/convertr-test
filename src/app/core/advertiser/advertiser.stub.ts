import { Model } from "../model/model";
import { AdvertiserData } from "./advertiserData";
import { AdvertiserVirtualData } from "./advertiserVirtualData";

export class AdvertiserStub implements Model<AdvertiserData> {
	public attributes: AdvertiserData;
	public virtualField: Partial<AdvertiserVirtualData>;
	public setVirtualField:(key: keyof AdvertiserVirtualData, value: string) => void = jasmine.createSpy('setVirtualFields');
}