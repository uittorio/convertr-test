import { AdvertiserData } from "./advertiserData";
import { AdvertiserVirtualData } from "./advertiserVirtualData";

export type AdvertiserViewData = AdvertiserData & AdvertiserVirtualData;