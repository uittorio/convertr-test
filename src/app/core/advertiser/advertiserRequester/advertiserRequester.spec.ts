import { async } from "@angular/core/testing";
import { ModelRequesterStub } from "../../model/modelRequester/modelRequester.stub";
import { AdvertiserFactoryStub } from "../factory/advertiserFactory.stub";
import { AdvertiserRequester } from "./advertiserRequester";
import { Advertiser } from "../advertiser";
import { AdvertiserStub } from "../advertiser.stub";

describe("AdvertiserRequester", () => {
	let advertiserRequester: AdvertiserRequester,
		modelRequester: ModelRequesterStub,
		advertiserFactory: AdvertiserFactoryStub;
	
	beforeEach(async(() => {
		modelRequester = new ModelRequesterStub();
		advertiserFactory = new AdvertiserFactoryStub();
		advertiserRequester = new AdvertiserRequester(modelRequester, advertiserFactory);
	}));
	
	describe("when getting advertisers", () => {
		let advertisersResult: Array<Advertiser>,
			advertisersFetched: Array<Advertiser>,
			resolveRequester: Function;
		
		beforeEach(async(() => {
			(modelRequester.getCollectionData as jasmine.Spy).and.returnValue(new Promise((resolve: Function) => {
				resolveRequester = resolve;
			}));
			
			advertiserRequester.list().then((advertisers: Array<Advertiser>) => {
				advertisersFetched = advertisers;
			});
			
			advertisersResult = [new AdvertiserStub()];
			
			resolveRequester(advertisersResult);
		}));
		
		it("should get data from the requester with no params", () => {
			expect(modelRequester.getCollectionData).toHaveBeenCalledWith(advertiserFactory, '/advertisers', {});
		});
		
		it("should return the advertisers", () => {
			expect(advertisersFetched).toBe(advertisersResult);
		});
	});
	
	describe("when creating an advertiser", () => {
		let advertiserResult: Advertiser,
			advertiserCreated: Advertiser,
			resolveRequester: Function;
		
		beforeEach(async(() => {
			(modelRequester.post as jasmine.Spy).and.returnValue(new Promise((resolve: Function) => {
				resolveRequester = resolve;
			}));
			
			advertiserRequester.create({
				name: 'name',
				orgurl: 'orgurl',
				address: 'address'
			}).then((advertiser: Advertiser) => {
				advertiserCreated = advertiser;
			});
			
			advertiserResult = new AdvertiserStub();
			
			resolveRequester(advertiserResult);
		}));
		
		it("should post the data to the requester with no params", () => {
			expect(modelRequester.post).toHaveBeenCalledWith(advertiserFactory, {
				name: 'name',
				orgurl: 'orgurl',
				address: 'address'
			}, '/advertisers', {});
		});
		
		it("should return the address", () => {
			expect(advertiserCreated).toBe(advertiserResult);
		});
	});
});