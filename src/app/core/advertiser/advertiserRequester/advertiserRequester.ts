import { ModelRequester } from "../../model/modelRequester/modelRequester";
import { AdvertiserFactory } from "../factory/advertiserFactory";
import { Advertiser } from "../advertiser";
import { Injectable } from "@angular/core";
import { AdvertiserData } from "../advertiserData";

@Injectable()
export class AdvertiserRequester {
	private readonly _advertiserListEndpoint: string = "/advertisers";
	private _advertiserFactory: AdvertiserFactory;
	private _modelRequester: ModelRequester;
	
	constructor(modelRequester: ModelRequester, accountFactory: AdvertiserFactory) {
		this._advertiserFactory = accountFactory;
		this._modelRequester = modelRequester;
	}
	
	public list(): Promise<Array<Advertiser>> {
		return this._modelRequester.getCollectionData(this._advertiserFactory, this._advertiserListEndpoint, {});
	}
	
	public create(advertiser: AdvertiserData): Promise<Advertiser> {
		return this._modelRequester.post(this._advertiserFactory, advertiser, this._advertiserListEndpoint, {});
	}
}
