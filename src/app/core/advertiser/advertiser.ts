import { Model } from "../model/model";
import { AdvertiserData } from "./advertiserData";
import { AdvertiserVirtualData } from "./advertiserVirtualData";

export class Advertiser implements Model<AdvertiserData> {
	public attributes: AdvertiserData;
	public virtualField: Partial<AdvertiserVirtualData>;
	
	public static create(data: AdvertiserData): Advertiser {
		return new Advertiser(data);
	}
	
	private constructor(data: AdvertiserData) {
		this.attributes = {
			name: data.name,
			orgurl: data.orgurl,
			address: data.address
		}
		this.virtualField = {};
	}
	
	public setVirtualField(key: keyof AdvertiserVirtualData, value: string): void {
		this.virtualField[key] = value;
	}
}