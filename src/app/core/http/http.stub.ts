import { Observable } from "rxjs/Observable";
import { IDict } from "../dictionary/dictionary";
import { HttpHeaders } from "@angular/common/http/src/headers";
import { IHttp } from "./http";

export class HttpStub implements IHttp {
	get: <T>(url: string, options?: {
		params: IDict<string>,
		headers?: HttpHeaders
	}) => Observable<T> = jasmine.createSpy('get');
	
	post: <T>(url: string, body:T, options?: {
		params: IDict<string>,
		headers?: HttpHeaders
	}) => Observable<T> = jasmine.createSpy('get');
}