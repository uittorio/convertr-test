import { Observable } from "rxjs/Observable";
import { IDict } from "../dictionary/dictionary";
import { HttpHeaders } from "@angular/common/http/src/headers";

export interface IHttp {
	get<T>(url: string, options?: {
		params: IDict<string>,
		headers?: HttpHeaders
	}): Observable<T>;
	
	post<T>(url: string, body:T, options?: {
		params: IDict<string>,
		headers?: HttpHeaders
	}): Observable<T>;
}