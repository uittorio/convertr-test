import { CRValidatorsService } from "./validatorService";
import { FormControl } from "@angular/forms";

describe("ValidatorServiceSpec", () => {
	it('should be invalid url for test', function () {
		const formControl: FormControl = new FormControl();
		formControl.setValue('test');
		expect(CRValidatorsService.url(formControl)).toEqual({
			url: {
				valid: false
			}
		});
	});
	
	it('should be invalid url for test', function () {
		const formControl: FormControl = new FormControl();
		formControl.setValue('test');
		expect(CRValidatorsService.url(formControl)).toEqual({
			url: {
				valid: false
			}
		});
	});
	
	it('should be invalid url for http:com', function () {
		const formControl: FormControl = new FormControl();
		formControl.setValue('http:com');
		expect(CRValidatorsService.url(formControl)).toEqual({
			url: {
				valid: false
			}
		});
	});
	
	it('should be invalid url for https:com', function () {
		const formControl: FormControl = new FormControl();
		formControl.setValue('https:com');
		expect(CRValidatorsService.url(formControl)).toEqual({
			url: {
				valid: false
			}
		});
	});
	
	it('should be valid url for http://test.com', function () {
		const formControl: FormControl = new FormControl();
		formControl.setValue('http://test.com');
		expect(CRValidatorsService.url(formControl)).toBe(null);
	});
	
	it('should be valid url for test.com', function () {
		const formControl: FormControl = new FormControl();
		formControl.setValue('test.com');
		expect(CRValidatorsService.url(formControl)).toBe(null);
	});
});