import { FormControl, Validators } from '@angular/forms';
import { Injectable } from "@angular/core";
import { Translation } from "../../shared/translate/translation";
import { IDict } from "../dictionary/dictionary";

@Injectable()
export class CRValidatorsService extends Validators {
	private static _validate(valid, type) {
		const result = {};
		
		result[type] = {
			valid: false
		};
		
		return valid ? null: result;
	}
	
	public static getValidatorTranslation(validatorValues: IDict<string>, validatorName: string): Translation {
		return {
			translateId: 'validators.' + validatorName,
			translateValues: validatorValues
		}
	}
	
	public static url(control: FormControl) {
		const REGEX = /^((?:(?:(?:ht|f)tps?):\/\/)?(?:\w+\.)+[a-zA-Z]+(?:\/\w+)*\/?(?:\?\w+=\w+(?:&\w+=\w+)*)?|ssh:\/\/(\w+\@(?:\w|\.)+\:[0-9]*)?(?:\/?\w+)*\/?)$/;
		
		return CRValidatorsService._validate(REGEX.test(control.value), 'url');
	}
}

