import { Graphic } from "../graphic";
import { GraphicName } from "../graphicName/graphicName";

export class GraphicRepositoryStub {
	public get: (imageName: GraphicName) => Graphic = jasmine.createSpy('GraphicRepository.get');
	public set: (imageName: Graphic) => void = jasmine.createSpy('GraphicRepository.get');
}