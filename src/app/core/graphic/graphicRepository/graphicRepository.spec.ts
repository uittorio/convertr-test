import { GraphicRepository } from "./graphicRepository";
import { GraphicName } from "../graphicName/graphicName";
import { Graphic } from "../graphic";
import { GraphicStub } from "../graphic.stub";

describe("GraphicRepository", () => {
	let graphicRepository: GraphicRepository;
	
	beforeEach(() => {
		graphicRepository = new GraphicRepository();
	});
	
	it('should get set images', function () {
		const graphic: Graphic = new GraphicStub();
		graphic.name = GraphicName.LOGO;
		graphicRepository.set(graphic);
		expect(graphicRepository.get(GraphicName.LOGO)).toBe(graphic);
	});
});