import { Graphic } from "../graphic";
import { GraphicName } from "../graphicName/graphicName";
import { Injectable } from "@angular/core";

@Injectable()
export class GraphicRepository {
	private _graphicList: Array<Graphic> = [];
	
	public get(imageName: GraphicName): Graphic {
		return this._graphicList[imageName];
	}
	
	public set(image: Graphic): void {
		this._graphicList[image.name] = image;
	}
}