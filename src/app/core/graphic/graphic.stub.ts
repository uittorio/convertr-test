import { GraphicName } from "./graphicName/graphicName";

export class GraphicStub {
	public name: GraphicName;
	public srcUrl: string;
}