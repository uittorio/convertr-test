import { GraphicName } from "./graphicName/graphicName";

export class Graphic {
	public name: GraphicName;
	public srcUrl: string;
	
	public static create(name: GraphicName, url: string): Graphic {
		return new Graphic(name, url);
	}
	
	private constructor(name: GraphicName, url: string) {
		this.name = name;
		this.srcUrl = url;
	}
}