import { GraphicName } from "../graphicName/graphicName";
import { Graphic } from "../graphic";
import { Injectable } from "@angular/core";

@Injectable()
export abstract class GraphicLoader {
	public abstract load(graphicName: GraphicName): Promise<Graphic>;
}