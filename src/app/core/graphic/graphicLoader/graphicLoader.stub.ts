import { GraphicName } from "../graphicName/graphicName";
import { Graphic } from "../graphic";

export class GraphicLoaderStub {
	public load: (graphicName: GraphicName) => Promise<Graphic> = jasmine.createSpy('load');
}