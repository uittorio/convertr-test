import { GraphicName } from "../graphicName/graphicName";

export const GraphicResources: { [key in GraphicName]: string } = {
	LOGO: "resources/images/logo.svg"
};
