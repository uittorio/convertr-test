import { Graphic } from "./graphic";
import { GraphicName } from "./graphicName/graphicName";

describe("Graphic", () => {
	let graphic: Graphic;
	
	it('should contains name and url', function () {
		graphic = Graphic.create(GraphicName.LOGO, 'url');
		
		expect(graphic.name).toBe(GraphicName.LOGO);
		expect(graphic.srcUrl).toBe('url');
	});
});