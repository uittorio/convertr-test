import { GraphicName } from "../graphicName/graphicName";

export const GraphicNamePreload: Array<GraphicName> = [
	GraphicName.LOGO
];