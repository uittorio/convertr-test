import { Component, OnInit } from "@angular/core";
import { GraphicRepository } from "../core/graphic/graphicRepository/graphicRepository";
import { GraphicName } from "../core/graphic/graphicName/graphicName";
import { Graphic } from "../core/graphic/graphic";
import { TableHeaderConfiguration } from "../shared/table/tableHeader/tableHeaderConfiguration";
import { AdvertiserData } from "../core/advertiser/advertiserData";
import { Advertiser } from "../core/advertiser/advertiser";
import { AdvertiserRequester } from "../core/advertiser/advertiserRequester/advertiserRequester";
import { TableRowValueConfiguration } from "../shared/table/tableRow/tableRowValueConfiguration";
import { AdvertiserTableData } from "../core/advertiser/advertiserTableData/advertiserTableData";
import { AddressRequester } from "../core/address/addressRequester/addressRequester";
import { Address } from "../core/address/address";
import { Subject } from "rxjs/Subject";
import { AdvertiserViewData } from "../core/advertiser/advertiserViewData";

@Component({
	selector: 'cr-home',
	templateUrl: './home.pug',
	styleUrls: ['./home.scss']
})
export class HomeComponent implements OnInit {
	public logo: Graphic;
	public isLoading: boolean;
	public configHeaders: Array<TableHeaderConfiguration<AdvertiserViewData>>;
	public configRows: Array<TableRowValueConfiguration>;
	private _advertiserModelRequester: AdvertiserRequester;
	private _advertiserTableData: AdvertiserTableData;
	private _addressRequester: AddressRequester;
	private openDialog: Subject<boolean>;
	
	constructor(graphicRepository: GraphicRepository,
							advertiserRequester: AdvertiserRequester,
							advertiserTableData: AdvertiserTableData,
							addressRequester: AddressRequester) {
		this._advertiserModelRequester = advertiserRequester;
		this._advertiserTableData = advertiserTableData;
		this._addressRequester = addressRequester;
		this.logo = graphicRepository.get(GraphicName.LOGO);
		this.isLoading = true;
		this.openDialog = new Subject<boolean>();
	}
	
	public ngOnInit(): void {
		this.configHeaders = [];
		this.configRows = [];
		this._getAdvertiser().then(() => {
			this.isLoading = false;
		});
	}
	
	public openAdvertiserDialog(): void {
		this.openDialog.next(true);
	}
	
	public addAdvertiser(advertiser: Advertiser): void {
		this.configRows = this.configRows.concat(this._advertiserTableData.generateTableDataRows(advertiser));
	}
	
	private _getAdvertiser(): Promise<void> {
		return this._advertiserModelRequester.list().then((advertiserList: Array<Advertiser>) => {
			const addressesPromise: Array<Promise<Address>> = [];
			
			advertiserList.forEach((advertiser: Advertiser) => {
				const addressPromise = this._addressRequester.get(advertiser.attributes.address);
				addressesPromise.push(addressPromise);
				
				addressPromise.then((address: Address) => {
					advertiser.setVirtualField('addressName', address.attributes.address);
					advertiser.setVirtualField('postcode', address.attributes.postcode);
				});
			});
			
			return Promise.all(addressesPromise).then(() => {
				this.configHeaders = this._advertiserTableData.tableDataHeaders;
				this.configRows = this._generateConfigRows(advertiserList);
			});
		})
	}
	
	private _generateConfigRows(advertiserList: Array<Advertiser>): Array<TableRowValueConfiguration> {
		return advertiserList.map((advertiser: Advertiser) => {
			return this._advertiserTableData.generateTableDataRows(advertiser);
		})
	}
}