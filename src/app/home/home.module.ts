import { NgModule } from '@angular/core';
import { SharedModule } from "../shared/shared.module";
import { HomeComponent } from "./home.component";
import { CoreModule } from "../core/core.module";

@NgModule({
	imports: [
		SharedModule,
		CoreModule
	],
	declarations: [
		HomeComponent
	]
})

export class HomeModule {}
