import { IDict } from "../../core/dictionary/dictionary";

export interface Translation {
	translateId: string;
	translateValues?: IDict<string>;
}