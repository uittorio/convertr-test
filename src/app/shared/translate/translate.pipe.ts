import { Pipe, PipeTransform } from "@angular/core";
import { Translation } from "./translation";
import { TranslateService } from "@ngx-translate/core";

@Pipe({ name: 'CRTranslate' })
export class Translate implements PipeTransform {
	private _translateService: TranslateService;
	
	constructor(translateService: TranslateService) {
		this._translateService = translateService;
	}
	
	public transform(translation: Translation): string {
		return this._translateService.instant(translation.translateId, translation.translateValues);
	}
}