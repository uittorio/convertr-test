import { Pipe, PipeTransform } from "@angular/core";
import { Translation } from "./translation";

@Pipe({ name: 'CRTranslate' })
export class TranslateStub implements PipeTransform {
	public transform(translation: Translation): string {
		return translation.translateId + (translation.translateValues || "");
	}
}