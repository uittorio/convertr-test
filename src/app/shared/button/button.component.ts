import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Translation } from "../translate/translation";

@Component({
	selector: 'cr-button',
	templateUrl: './button.pug',
	styleUrls: ['./button.scss']
})

export class ButtonComponent {
	@Input()
	public crButtonLabel: Translation;
	
	@Input()
	public crButtonDisabled: boolean;
	
	@Output()
	public crButtonOnAction: EventEmitter<void> = new EventEmitter<void>();
}