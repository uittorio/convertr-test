import { Component, forwardRef } from '@angular/core';
import { FormComponent } from "../form/form.component";

@Component({
	selector: 'cr-custom-form',
	templateUrl: '../form/form.pug',
	styleUrls: ['./customForm.scss']
})

export class CustomFormComponent extends FormComponent {}