import { Component, forwardRef } from '@angular/core';
import { FormInputComponent } from "../../form/formInput/formInput.component";

@Component({
	selector: 'cr-custom-form-input',
	templateUrl: '../../form/formInput/formInput.pug',
	styleUrls: ['./customFormInput.scss'],
	providers: [
		{
			provide: FormInputComponent,
			useExisting: forwardRef(() => CustomFormInputComponent)
		}]
})

export class CustomFormInputComponent extends FormInputComponent {
}
