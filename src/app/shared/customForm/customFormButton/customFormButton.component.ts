import { Component, forwardRef } from '@angular/core';
import { FormInputComponent } from "../../form/formInput/formInput.component";
import { GraphicRepository } from "../../../core/graphic/graphicRepository/graphicRepository";
import { FormButtonComponent } from "../../form/formButton/formButton.component";

@Component({
	selector: 'cr-custom-form-button',
	templateUrl: '../../form/formButton/formButton.pug',
	styleUrls: ['./customFormButton.scss'],
	providers: [
		{
			provide: FormButtonComponent,
			useExisting: forwardRef(() => CustomFormButtonComponent)
		}]
})

export class CustomFormButtonComponent extends FormButtonComponent {}
