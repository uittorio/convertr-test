import { Component, forwardRef } from '@angular/core';
import { FormInputErrorsComponent } from "../../form/formInputErrors/formInputErrors.component";

@Component({
	selector: 'cr-custom-form-input-errors',
	templateUrl: '../../form/formInputErrors/formInputErrors.pug',
	styleUrls: ['./customFormInputErrors.scss'],
	providers: [
		{
			provide: FormInputErrorsComponent,
			useExisting: forwardRef(() => CustomFormInputErrorsComponent)
		}]
})

export class CustomFormInputErrorsComponent extends FormInputErrorsComponent {
}
