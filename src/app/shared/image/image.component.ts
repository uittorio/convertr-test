import { Component, Input } from "@angular/core";
import { Graphic } from "../../core/graphic/graphic";

@Component({
	selector: 'cr-image',
	templateUrl: './image.pug',
	styleUrls: ['./image.scss']
})
export class ImageComponent {
	@Input()
	public crImage: Graphic;
}