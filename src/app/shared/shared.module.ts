import { NgModule } from '@angular/core';
import { ImageComponent } from "./image/image.component";
import { Translate } from "./translate/translate.pipe";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CustomFormComponent } from "./customForm/customForm";
import { CustomFormInputComponent } from "./customForm/customFormInput/customFormInput.component";
import { CustomFormInputErrorsComponent } from "./customForm/customFormInputErrors/customFormInputErrors.component";
import { TableComponent } from "./table/table.component";
import { LoadingSpinnerComponent } from "./loading-spinner/loading-spinner.component";
import { ButtonComponent } from "./button/button.component";
import { CustomFormButtonComponent } from "./customForm/customFormButton/customFormButton.component";

@NgModule({
	imports: [
		RouterModule,
		CommonModule,
		FormsModule,
		ReactiveFormsModule
	],
	declarations: [
		ImageComponent,
		Translate,
		CustomFormComponent,
		CustomFormInputComponent,
		CustomFormInputErrorsComponent,
		CustomFormButtonComponent,
		TableComponent,
		LoadingSpinnerComponent,
		ButtonComponent
	],
	exports: [
		RouterModule,
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
		ImageComponent,
		Translate,
		CustomFormComponent,
		CustomFormInputComponent,
		CustomFormInputErrorsComponent,
		CustomFormButtonComponent,
		TableComponent,
		LoadingSpinnerComponent,
		ButtonComponent
	]
})

export class SharedModule {

}
