import { Translation } from "../../translate/translation";

export interface TableHeaderConfiguration<TKey> {
	key: keyof TKey;
	value: Translation;
}