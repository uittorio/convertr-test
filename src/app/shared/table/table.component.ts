import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { TableHeaderConfiguration } from "./tableHeader/tableHeaderConfiguration";
import { orderBy, values } from "lodash";
import { TableRowValueConfiguration } from "./tableRow/tableRowValueConfiguration";

@Component({
	selector: 'cr-table',
	templateUrl: './table.pug',
	styleUrls: ['./table.scss']
})
export class TableComponent<TKey> {
	public rowGroups: Array<Array<TableRowValueConfiguration>>;
	@Input()
	public crTableConfigurationHeaders: Array<TableHeaderConfiguration<TKey>>;
	private _originalRowGroups: Array<TableRowValueConfiguration>;
	
	@Input()
	public set crTableConfigurationRows(rowGroups: Array<TableRowValueConfiguration>) {
		this._originalRowGroups = rowGroups;
		if (rowGroups) {
			this._orderRows();
		}
	}
	
	private _orderRows(): void {
		this.rowGroups = this._originalRowGroups.map((rowGroups: TableRowValueConfiguration) => {
			return values(rowGroups);
		});
	}
}
