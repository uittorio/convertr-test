import { IDict } from "../../../core/dictionary/dictionary";

export interface TableRowValueConfiguration extends IDict<string | number> {}