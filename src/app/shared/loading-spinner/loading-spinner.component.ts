import { Component } from '@angular/core';

@Component({
  selector: 'cr-loading-spinner',
  templateUrl: './loading-spinner.pug',
  styleUrls: ['./loading-spinner.scss']
})

export class LoadingSpinnerComponent {}