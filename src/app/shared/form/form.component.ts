import {
  Component,
  Input,
  QueryList,
  AfterViewInit,
  OnInit,
  ContentChildren
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { FormInputComponent } from './formInput/formInput.component';
import { FormInputErrorsComponent } from "./formInputErrors/formInputErrors.component";
import { FormButtonComponent } from "./formButton/formButton.component";
import { FormConfiguration } from "./formConfiguration";

export abstract class FormComponent implements OnInit, AfterViewInit {
	@Input()
	public crFormConfiguration: FormConfiguration;
	
  @ContentChildren(FormInputComponent)
  private _inputs: QueryList<FormInputComponent>;
	
	@ContentChildren(FormInputErrorsComponent)
	private _inputErrors: QueryList<FormInputErrorsComponent>;
	
	@ContentChildren(FormButtonComponent)
	private _buttons: QueryList<FormButtonComponent>;

	form: FormGroup;

	public ngOnInit(): void {
    this.form = new FormGroup({});
	}

	public ngAfterViewInit(): void {
	  setTimeout(() => {
			this._inputs.forEach((input: FormInputComponent) => {
				const formControl = new FormControl(input.crFormInputConfiguration.value, Validators.compose(input.crFormInputConfiguration.validators));
			
				this.form.addControl(input.crFormInputConfiguration.name, formControl);
			
				input.setControl(formControl);
			});
		
			this._inputErrors.forEach((error: FormInputErrorsComponent) => {
				error.setControl(this.form.controls[error.crFormInputErrorsConfiguration.name]);
			});
			
			this._buttons.forEach((button: FormButtonComponent) => {
				button.setForm(this.form);
			});
    });
  }
	
	public onSubmit(form: FormGroup): void {
		if (form.valid) {
			this.crFormConfiguration.submit(form);
		} else {
			this._inputErrors.forEach((error: FormInputErrorsComponent) => {
				error.setErrors();
			});
		}
		
	}
}
