import { FormInputConfiguration } from './formInput/formInputConfiguration';
import { FormGroup } from "@angular/forms";

export interface FormConfiguration {
  inputs: Array<FormInputConfiguration>;
  submit(form: FormGroup): void;
}
