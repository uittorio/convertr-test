import { Input } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { FormInputConfiguration } from '../formInput/formInputConfiguration';
import { map } from 'lodash';
import { Translation } from "../../translate/translation";
import { CRValidatorsService } from "../../../core/validators/validatorService";
import { IDict } from "../../../core/dictionary/dictionary";

export abstract class FormInputErrorsComponent {
	@Input()
	public crFormInputErrorsConfiguration: FormInputConfiguration;
	
	public control: AbstractControl;
	public errors: Array<Translation>;
	
	public setControl(control: AbstractControl) {
		this.control = control;
		
		this.control.valueChanges.subscribe(
			() => {
				this.setErrors();
			}
		);
	}
	
	public setErrors(): void {
		this.errors = map(this.control.errors, (validatorValues: IDict<string>, validatorName: string) => {
			return CRValidatorsService.getValidatorTranslation(validatorValues, validatorName);
		});
	}
}
