import { Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { FormInputConfiguration } from './formInputConfiguration';

export abstract class FormInputComponent {
	@Input()
	public crFormInputConfiguration: FormInputConfiguration;
	
	public control: AbstractControl;
	
	public setControl(control: FormControl) {
		this.control = control;
	}
}
