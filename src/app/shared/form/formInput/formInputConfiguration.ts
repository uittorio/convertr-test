import { ValidatorFn } from '@angular/forms';
import { Translation } from "../../translate/translation";
import { GraphicName } from "../../../core/graphic/graphicName/graphicName";

export interface FormInputConfiguration {
  value: string;
  type: string;
  name: string;
  placeholder: Translation;
  validators?: Array<ValidatorFn>;
}
