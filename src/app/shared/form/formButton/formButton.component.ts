import { Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Translation } from "../../translate/translation";

export class FormButtonComponent {
  @Input()
  public crFormButtonLabel: Translation;
  
  public form: FormGroup;

  public setForm(form: FormGroup) {
    this.form = form;
  }
}